from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import UserLoginForm, UserRegisterForm
from . import urls
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout, get_user_model

# Create your views here.

@login_required
def home(request):
    name = request.user
    username = name.username
    return render(request, 'home.html', {"username":username})

def login(request):
    if request.method == "GET":
        form = UserLoginForm()
        return render(request, 'login.html', {"form":form})
    elif request.method == "POST":
        form = UserLoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(username=username, password=password)
            if user is not None:
                auth_login(request, user)
                return redirect('home')
        return redirect('/')


def logout(request):
    auth_logout(request)
    return redirect('login')

def register(request):
    if request.method == "GET":
        form = UserRegisterForm()
        return render(request, 'register.html', {"form":form})
    elif request.method == "POST":
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            password = form.cleaned_data.get("password")
            user.set_password(password)
            user.save()
            return redirect('login')
        return redirect('register')