from django.test import TestCase, LiveServerTestCase
from django.urls import reverse
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.action_chains import ActionChains

from .models import *
from .forms import *
# Create your tests here.
class UnitTest(TestCase):
    
    def test_lobby_page(self):
        url = reverse('home')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 302)
    
    def test_login_page(self):
        url = reverse('login')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'login.html')
    
    def test_logout_page(self):
        url = reverse('logout')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 302)

class FunctionalTest(LiveServerTestCase):
    
    def setUp(self):
        options = Options()
        options.add_argument("--headless")
        self.selenium = webdriver.Firefox(firefox_options=options)
        super(FunctionalTest, self).setUp()
    
    def test_logging_in_success(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        username = selenium.find_element_by_id("id_username")
        password = selenium.find_element_by_id("id_password")
        signIn = selenium.find_element_by_id("id_login")
        username.send_keys("eric_a")
        password.send_keys("let me in, LET ME IN")
        signIn.click()
        selenium.implicitly_wait(10)
        assert 'Lobby' not in selenium.page_source

        register = selenium.find_element_by_id("id_register")
        register.click()
        selenium.implicitly_wait(10)
        username = selenium.find_element_by_id("id_username")
        password = selenium.find_element_by_id("id_password")
        register = selenium.find_element_by_id("id_register")
        username.send_keys("eric_a")
        password.send_keys("let me in, LET ME IN")
        register.click()
        selenium.implicitly_wait(10)
        assert 'Log In' in selenium.page_source

        username = selenium.find_element_by_id("id_username")
        password = selenium.find_element_by_id("id_password")
        signIn = selenium.find_element_by_id("id_login")
        username.send_keys("eric_a")
        password.send_keys("let me in, LET ME IN")
        signIn.click()
        selenium.implicitly_wait(10)
        assert 'Lobby' in selenium.page_source

        signOut = selenium.find_element_by_id("id_logout")
        signOut.click()
        selenium.implicitly_wait(10)
        assert 'Log In' in selenium.page_source
    
    def test_logging_in_wrong_password(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        register = selenium.find_element_by_id("id_register")
        register.click()
        selenium.implicitly_wait(10)

        username = selenium.find_element_by_id("id_username")
        password = selenium.find_element_by_id("id_password")
        register = selenium.find_element_by_id("id_register")
        username.send_keys("eric_a")
        password.send_keys("let me in, LET ME IN")
        register.click()
        selenium.implicitly_wait(10)

        username = selenium.find_element_by_id("id_username")
        password = selenium.find_element_by_id("id_password")
        signIn = selenium.find_element_by_id("id_login")
        username.send_keys("eric_a")
        password.send_keys("Im totally eric_a")
        signIn.click()
        selenium.implicitly_wait(10)
        assert 'Lobby' not in selenium.page_source
        assert 'Log In' in selenium.page_source
    
    def test_register_same_username(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)

        register = selenium.find_element_by_id("id_register")
        register.click()
        selenium.implicitly_wait(10)

        username = selenium.find_element_by_id("id_username")
        password = selenium.find_element_by_id("id_password")
        register = selenium.find_element_by_id("id_register")
        username.send_keys("eric_a")
        password.send_keys("let me in, LET ME IN")
        register.click()
        selenium.implicitly_wait(10)

        register = selenium.find_element_by_id("id_register")
        register.click()
        selenium.implicitly_wait(10)

        username = selenium.find_element_by_id("id_username")
        password = selenium.find_element_by_id("id_password")
        register = selenium.find_element_by_id("id_register")
        username.send_keys("eric_a")
        password.send_keys("let me in, LET ME IN")
        register.click()
        selenium.implicitly_wait(10)
        assert "Log In" not in selenium.page_source
        assert "Register" in selenium.page_source


    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()