from django import forms
from django.contrib.auth import authenticate,get_user_model

User = get_user_model()

class UserLoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self, *args, **kwargs):
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']
        
        if username and password:
            user = authenticate(username=username, password=password)
            if not user:
                usernames = User.objects.filter(username=username)
                if usernames.exists():
                    raise forms.ValidationError("Wrong password")
                else:
                    raise forms.ValidationError("This user doesn't exist")
            return super(UserLoginForm, self).clean(*args, **kwargs)

class UserRegisterForm(forms.ModelForm):

    class Meta:
        model = User
        fields = [
            'username',
            'password'
        ]

    def clean_username(self):
        username = self.cleaned_data['username']
        usernames = User.objects.filter(username=username)
        if usernames.exists():
            raise forms.ValidationError("This username is not available")
        return username